package ez.calculator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
/**
 * A program that allows users to make calculations.
 * @author nwh212
 */
public class EZCalculator extends JFrame {
    private JPanel mainPanel;
    private JTextField textField;
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JButton button7;
    private JButton button8;
    private JButton button9;
    private JButton button0;
    
    private JButton buttondot;
    private JButton buttontobin;
    private JButton buttontohex;
    private JButton buttonhextodec;
    private JButton buttonbintodec;
    private JButton buttonsquare;
    
    private JButton buttonc;
    private JButton buttonplus;
    private JButton buttonminus;
    private JButton buttontimes;
    private JButton buttondivide;
    private JButton buttonequals;
    
    private int plusCheck = 0;
    private int minusCheck = 0;
    private int timesCheck = 0;
    private int divideCheck = 0;
    
    
    private double firstVal;
    
    ImageIcon img = new ImageIcon("Calc.png");
    
    GridBagConstraints c = new GridBagConstraints();

    
    public EZCalculator() {
        setTitle("EZ Calculator");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setupMainPanel();
        setLayout(new BorderLayout());
        add(mainPanel, BorderLayout.CENTER);
        
        pack();
        setVisible(true);
    }
    
    private void setupMainPanel() {
        mainPanel = new JPanel(new GridBagLayout());
        
        textField = new JTextField();
        textField.setFont(new Font("Arial", Font.PLAIN, 20));
        c.gridwidth = 5;
        c.insets = new Insets(2,2,2,2);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        mainPanel.add(textField, c);
        c.gridwidth = 1;
        
        button1 = new JButton("1");
        button2 = new JButton("2");
        button3 = new JButton("3");
        button4 = new JButton("4");
        button5 = new JButton("5");
        button6 = new JButton("6");
        button7 = new JButton("7");
        button8 = new JButton("8");
        button9 = new JButton("9");
        button0 = new JButton("0");
        buttondot = new JButton(".");
        buttontobin = new JButton("DecToBin");
        buttontohex = new JButton("DecToHex");
        buttonhextodec = new JButton("HexToDec");
        buttonbintodec = new JButton("BinToDec");
        buttonsquare = new JButton("x²");
        buttonc = new JButton("C");
        buttonplus = new JButton("+");
        buttonminus = new JButton("-");
        buttontimes = new JButton("×");
        buttondivide = new JButton("÷");
        buttonequals = new JButton("=");
        button1.addActionListener(new One());
        button2.addActionListener(new Two());
        button3.addActionListener(new Three());
        button4.addActionListener(new Four());
        button5.addActionListener(new Five());
        button6.addActionListener(new Six());
        button7.addActionListener(new Seven());
        button8.addActionListener(new Eight());
        button9.addActionListener(new Nine());
        button0.addActionListener(new Zero());
        buttondot.addActionListener(new Dot());
        buttontobin.addActionListener(new ToBin());
        buttontohex.addActionListener(new ToHex());
        buttonhextodec.addActionListener(new HexToDec());
        buttonbintodec.addActionListener(new BinToDec());
        buttonc.addActionListener(new Clear());
        buttonplus.addActionListener(new Plus());
        buttonminus.addActionListener(new Minus());
        buttontimes.addActionListener(new Times());
        buttondivide.addActionListener(new Divide());
        buttonequals.addActionListener(new Equals());
        buttonsquare.addActionListener(new Square());
        c.weightx = 0;
        c.gridx = 0;
        c.gridy = 1;
        c.insets = new Insets(0,2,0,0);
        mainPanel.add(button1, c);
        c.gridx = 1;
        c.gridy = 1;
        c.insets = new Insets(0,0,0,0);
        mainPanel.add(button2, c);
        c.gridx = 2;
        c.gridy = 1;
        mainPanel.add(button3, c);
        c.gridx = 0;
        c.gridy = 2;
        c.insets = new Insets(0,2,0,0);
        mainPanel.add(button4, c);
        c.gridx = 1;
        c.gridy = 2;
        c.insets = new Insets(0,0,0,0);
        mainPanel.add(button5, c);
        c.gridx = 2;
        c.gridy = 2;
        mainPanel.add(button6, c);
        c.gridx = 0;
        c.gridy = 3;
        c.insets = new Insets(0,2,0,0);
        mainPanel.add(button7, c);
        c.insets = new Insets(0,0,0,0);
        c.gridx = 1;
        c.gridy = 3;
        mainPanel.add(button8, c);
        c.gridx = 2;
        c.gridy = 3;
        mainPanel.add(button9, c);
        c.gridx = 0;
        c.gridy = 4;
        c.insets = new Insets(0,2,0,0);
        mainPanel.add(button0, c);
        c.gridx = 3;
        c.gridy = 1;
        c.insets = new Insets(0,2,0,2);
        mainPanel.add(buttonplus, c);
        c.gridx = 3;
        c.gridy = 3;
        c.insets = new Insets(0,2,0,2);
        mainPanel.add(buttontimes, c);
        c.gridx = 2;
        c.gridy = 4;
        c.insets = new Insets(0,0,0,0);
        mainPanel.add(buttonequals, c);
        c.gridx = 1;
        c.gridy = 4;
        c.insets = new Insets(0,0,0,0);
        mainPanel.add(buttondot, c);
        c.gridx = 3;
        c.gridy = 2;
        c.insets = new Insets(0,2,0,2);
        mainPanel.add(buttonminus, c);
        c.gridx = 3;
        c.gridy = 4;
        c.insets = new Insets(0,2,0,2);
        mainPanel.add(buttondivide, c);
        c.gridx = 3;
        c.gridy = 5;
        c.insets = new Insets(0,2,2,2);
        mainPanel.add(buttonsquare, c);
        
        c.insets = new Insets(0,2,2,0);
        c.gridwidth = 3;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 5;
        mainPanel.add(buttonc, c);
        c.gridx = 4;
        c.gridy = 1;
        c.gridwidth = 1;
        c.insets = new Insets(0,0,0,2);
        mainPanel.add(buttontobin, c);
        c.gridx = 4;
        c.gridy = 2;
        mainPanel.add(buttonbintodec, c);
        c.gridx = 4;
        c.gridy = 3;
        c.insets = new Insets(2,0,0,2);
        mainPanel.add(buttontohex, c);
        c.gridx = 4;
        c.gridy = 4;
        c.insets = new Insets(0,0,0,2);
        mainPanel.add(buttonhextodec, c);
        
        pack();
    }
    
    private class One implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            textField.setText(textField.getText() + "1");
            pack();
        }
    }
    private class Two implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            textField.setText(textField.getText() + "2");
            pack();
        }
    }
    private class Three implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            textField.setText(textField.getText() + "3");
            pack();
        }
    }
    private class Four implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            textField.setText(textField.getText() + "4");
            pack();
        }
    }
    private class Five implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            textField.setText(textField.getText() + "5");
            pack();
        }
    }
    private class Six implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            textField.setText(textField.getText() + "6");
            pack();
        }
    }
    private class Seven implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            textField.setText(textField.getText() + "7");
            pack();
        }
    }
    private class Eight implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            textField.setText(textField.getText() + "8");
            pack();
        }
    }
    private class Nine implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            textField.setText(textField.getText() + "9");
            pack();
        }
    }
    private class Zero implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            textField.setText(textField.getText() + "0");
            pack();
        }
    }
    private class Dot implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            textField.setText(textField.getText() + ".");
            pack();
        }
    }
    private class Clear implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            textField.setText("");
            pack();
        }
    }
    private class Plus implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            plusCheck = 1;
            minusCheck = 0;
            timesCheck = 0;
            divideCheck = 0;
            if(textField.getText().equals("")) {
            } 
            else {
                firstVal = Double.parseDouble(textField.getText());
                textField.setText("");
                pack();
            }
        }
    }
    private class Minus implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            plusCheck = 0;
            minusCheck = 1;
            timesCheck = 0;
            divideCheck = 0;
            if(textField.getText().equals("")) {
            } 
            else {
                firstVal = Double.parseDouble(textField.getText());
                textField.setText("");
                pack();
            }
        }
    }
    private class Times implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            plusCheck = 0;
            minusCheck = 0;
            timesCheck = 1;
            divideCheck = 0;
            if(textField.getText().equals("")) {
            } 
            else {
                firstVal = Double.parseDouble(textField.getText());
                textField.setText("");
                pack();
            }
        }
    }
    private class Divide implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            plusCheck = 0;
            minusCheck = 0;
            timesCheck = 0;
            divideCheck = 1;
            if(textField.getText().equals("")) {
            } 
            else {
                firstVal = Double.parseDouble(textField.getText());
                textField.setText("");
                pack();
            }
        }
    }
    private class Square implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            plusCheck = 0;
            minusCheck = 0;
            timesCheck = 0;
            divideCheck = 0;
            if(textField.getText().equals("")) {
            } 
            else {
                Double.parseDouble(textField.getText());
                textField.setText(Double.toString(Double.parseDouble(textField.getText()) * Double.parseDouble(textField.getText())));
                pack();
            }
        }
    }
    private class Equals implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if(textField.getText().equals("")) {
            } 
            else if (plusCheck == 1) {
                plusCheck = 0;
                double total = firstVal + Double.parseDouble(textField.getText());
                textField.setText("" + total);
                pack();
            }
            else if (minusCheck == 1) {
                minusCheck = 0;
                double total = firstVal - Double.parseDouble(textField.getText());
                textField.setText("" + total);
                pack();
            }
            else if (timesCheck == 1) {
                plusCheck = 0;
                double total = firstVal * Double.parseDouble(textField.getText());
                textField.setText("" + total);
                pack();
            }
            else if (divideCheck == 1) {
                minusCheck = 0;
                double total = firstVal / Double.parseDouble(textField.getText());
                textField.setText("" + total);
                pack();
            }
        }
    }
    private class ToBin implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            textField.setText(Integer.toString(Integer.parseInt(textField.getText()),2));
            pack();
        }
    }
    private class ToHex implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            textField.setText(Integer.toString(Integer.parseInt(textField.getText()),16));
            pack();
        }
    }
    private class HexToDec implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            textField.setText(Integer.toString(Integer.parseInt(textField.getText(),16)));
            pack();
        }
    }
    private class BinToDec implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            textField.setText(Integer.toString(Integer.parseInt(textField.getText(),2)));
            pack();
        }
    }
    
    public static void main(String[] args) {
        new EZCalculator();
    }
}
